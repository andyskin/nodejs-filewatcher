import * as fs from 'fs';
import { EventEmitter } from 'events';

export class DirWatcher extends EventEmitter {
    private path: string;
    private delay: number;
    private filesMap: Map<string, number>;
    private watcher: NodeJS.Timeout | undefined;

    constructor(path: string, delay: number) {
        super();
        this.path = path;
        this.delay = delay;
        this.filesMap = new Map();
        this.watcher = undefined;

        this.watch();
    }

    /**
     * Reads all the files in the given folder with the required period of time
     *
     */
    watch() {
        let isPreviosCheckFinished = true;

        this.watcher = setInterval(() => {
            if (!isPreviosCheckFinished) return;
            isPreviosCheckFinished = false;

            fs.readdir(this.path, (error, fileNames) => {
                if (error) throw error;
                this.updateFilesMap(this.filterFiles(fileNames));
                isPreviosCheckFinished = true;
            });
            
        }, this.delay);
    }

    /**
     * Disables the filewatcher
     */
    private stop() {
        if (this.watcher) {
            this.watcher.unref();
            this.watcher = undefined;
            console.info(`The watcher for ${this.path} is disabled`);
        } else {
            console.warn(`No watcher is found. Please use the 'watch()' method to start a watcher.`);
        }
    }

    /**
     * Filters out non-CSV files
     * @param fileNames The list of files from FS
     * @returns The list of CSV filenames
     */
    private filterFiles(fileNames: string[]): string[] {
        return fileNames.filter(
            fileName => fileName.match(/.+\.csv/gm)
        );
    }

    /**
     * Clears the map of the files that were removed from the hard drive
     * @param fileNames The list of files from FS
     */
    private removeOutdatedFiles(fileNames: string[]): void {        
        for (const file of Array.from(this.filesMap.keys())) {
            if (!fileNames.includes(file)) {
                this.emit('changed', file, true);
                this.filesMap.delete(file);
            }
        }
    }

    /**
     * Updates the map with the information about updated files
     * @param fileNames The list of files from FS
     */
    private updateFilesMap(fileNames: string[]): void {
        this.removeOutdatedFiles(fileNames);
        
        for (const file of fileNames) {
            const mapField = this.filesMap.get(file),
                fileStat = fs.statSync(`${this.path}/${file}`);

            if (!mapField || mapField != fileStat.mtimeMs) {
                this.emit('changed', file);
                this.filesMap.set(file, fileStat.mtimeMs);
            }
        }
    }
}