import { readFile, readFileSync } from 'fs';
import { promisify } from 'util';
import { default as csvToJson } from 'csvtojson';
import { Converter as AsyncConverter } from 'csvtojson/v2/Converter';
import { encoding } from '../config/config.json';

const syncConverter = require('csvjson');
const readFileAsync = promisify(readFile);

export class Importer {
    constructor() { }

    async import(path: string) {
        const converter: AsyncConverter = csvToJson({
            noheader: true,
            output: "json"
        });

        return converter.fromString(
            await readFileAsync(path, encoding)
        );
    }

    importSync(path: string) {
        return syncConverter.toObject(
            readFileSync(path, encoding)
        );
    }

    importString(input: string) {
        return syncConverter.toObject(input);
    }
}
