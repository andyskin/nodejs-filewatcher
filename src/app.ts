import config from './config/config.json';
import { DirWatcher, Importer } from './services';

(function () {
    const allImports = new Map();
    const dirWatcher = new DirWatcher(config.dataFolder, 5000);
    const importer = new Importer();

    try {
        dirWatcher.on('changed', async (fileName: string, isRemoved?: boolean) => {
            if (isRemoved && allImports.get(fileName)) {
                allImports.delete(fileName);
            } else {
                const importedData = importer.importSync(`${config.dataFolder}/${fileName}`);
                allImports.set(fileName, importedData);
            }
            console.log(allImports);
        });
    } catch (error) {
        console.error(error);
    }
})();
