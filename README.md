# nodejs-filewatcher

The application automatically transforms data from added/modified CSV files into JSON format

## Usage
1. Install all dependencies
2. Modify the *config.json* file if required
3. Start an application:
```
npm start
```

## Workflow
The application creates a custom watcher for the directory that is set in *config.json* file (*/data* by default)
The watcher uses the FS module to collect the information (the date of the last change) about all the files in the folder
Once some file is added/modified/removed, the watcher fires an event and starts the import process

## Dependencies
* The application is built with **TypeScript** to avoid and type-related errors
* **csvjson** package handles the import in a synchronous way
* **csvtojson** is added to support an async import

## Further development
It is planned to store the imported data in actual JSON files via streams